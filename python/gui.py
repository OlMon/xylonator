# Import the necessary packages
import SheetVision.main as sv
import sys
import os
import argparse
import subprocess
import queue
import threading

from tkinter import Tk, Label, Entry, Button, PhotoImage, Checkbutton, StringVar, IntVar, filedialog, W, N, E, S, messagebox
from config import xylonator as config
from converter import *

class XylonatorGUI:
    def __init__(self, master, w, h):
        """
        Init xylonator gui.
        """
        self.interact = []
        self.master = master
        self.master.geometry(str(w)+'x'+str(h)+'+100+10')
        self.master.title("Xylonator GUI")
        self.master.configure(background='white')

        # Create headline
        self.label = Label(master, text="Xylonator GUI", font='Helvetica 14 bold')
        self.label.grid(columnspan=4, column=0, row=0, padx=5, pady=5, sticky=W)
        self.label.configure(background='white')
        
        # Set top image as background
        self.main_image = PhotoImage(file="kuka.png")
        self.main_image_label = Label(master, image=self.main_image)
        self.main_image_label.grid(columnspan=4, column=0, row=1, sticky=W+E+N+S)

        # Create headline
        self.label = Label(master, text="Configuration:", font='Helvetica 10 bold')
        self.label.grid(columnspan=4, column=0, row=2, padx=5, pady=5, sticky=W)
        self.label.configure(background='white')
                
        # Create browse button
        self.label = Label(master, text="Path")
        self.label.grid(column=0, row=3, padx=5, pady=5, sticky=W)
        self.label.configure(background='white')
        self.path_text = StringVar()
        self.path_field = Entry(master, textvariable=self.path_text)
        self.interact.append(self.path_field)
        self.path_field.grid(columnspan=1, column=1, row=3, padx=5, pady=5, sticky=W+E+N+S)
        self.open_button = Button(master, text="Browse", command=self.open_file)
        self.interact.append(self.open_button)
        self.open_button.grid(columnspan=2, column=2, row=3, padx=5, pady=5, sticky=W)
                
        # Create output browse button
        self.output_path_label = Label(master, text="Output Path")
        self.output_path_label.grid(column=0, row=4, padx=5, pady=5, sticky=W)
        self.output_path_label.configure(background='white')
        self.output_path_text = StringVar()
        self.output_path_field = Entry(master, textvariable=self.output_path_text)
        self.interact.append(self.output_path_field)
        self.output_path_field.grid(columnspan=1, column=1, row=4, padx=5, pady=5, sticky=W+E+N+S)
        self.output_open_button = Button(master, text="Browse", command=self.open_directory)
        self.interact.append(self.output_open_button)
        self.output_open_button.grid(columnspan=2, column=2, row=4, padx=5, pady=5, sticky=W)

        # Show checkbox
        self.midi_checked = IntVar()
        self.midi_checkbox = Checkbutton(master, text="Save midi file", onvalue = 1, offvalue = 0, variable=self.midi_checked)
        self.interact.append(self.midi_checkbox)
        self.midi_checkbox.configure(background='white')
        self.midi_checkbox.grid(columnspan=2, column=1, row=5, padx=5, pady=0, sticky=W)
       
        # Show play output checkbox
        self.output_midi_checked = IntVar()
        self.output_midi_checkbox = Checkbutton(master, text="Play output file", onvalue = 1, offvalue = 0, variable=self.output_midi_checked)
        self.interact.append(self.output_midi_checkbox)
        self.output_midi_checkbox.configure(background='white')
        self.output_midi_checkbox.grid(columnspan=2, column=1, row=6, padx=5, pady=0, sticky=W)
       
        # Create close and update button
        self.loading_msg = Label(master, text="")
        self.loading_msg.grid(column=0, row=7, padx=5, pady=5, sticky=E)
        self.play_button = Button(master, text="Play", command=self.play_file)
        self.interact.append(self.play_button)
        self.play_button.grid(column=1, row=7, padx=5, pady=5, sticky=E)
        self.convert_button = Button(master, text="Convert", command=self.convert_file)
        self.interact.append(self.convert_button)
        self.convert_button.grid(column=2, row=7, padx=5, pady=5, sticky=W)
        self.close_button = Button(master, text="Close", command=master.quit)
        self.interact.append(self.close_button)
        self.close_button.grid(column=3, row=7, padx=5, pady=5, sticky=E)
       
        # Set column size
        self.master.grid_columnconfigure(1, weight=5)
        
        # Initialize vars
        self.init_vars()
       
    def init_vars(self):
        """
        Initialize gui vars.
        """
        # Set default output directory
        pwd = os.getcwd()
        output_dir = os.path.join(os.path.dirname(pwd), "output")
        self.output_path_text.set(output_dir)
        self.queue = queue.Queue(10)
        
        # Set "same midi" as default
        self.midi_checked.set(1)
        
    def open_files(self, path):
        """
        Open file with default associated program.
        
        Args: 
            path: path to file
        """
        # Check if file exists and is a file
        if os.path.isfile(path):
            # Open file with default application
            os.startfile(path)
        else:
            messagebox.showinfo("Error", "Output file {} not found".format(path))         

    def play_file(self):
        """
        Play input or output file.
        """
        # Check if input file is loaded
        if self.path_text.get() != "":
            # Check file extension, only midi files can be played
            filename, file_extension = os.path.splitext(self.path_text.get())
            if not self.output_midi_checked.get() and file_extension != ".mid":
                messagebox.showinfo("Error", "Wrong file format. Only MIDI files can be played")
            else:
                # Play file
                if not self.output_midi_checked.get():
                    # Open input file
                    self.open_files(self.path_text.get())
                else:
                    # Build path to output file, file must have the same name
                    output_file = os.path.join(self.output_path_text.get(), 
                        os.path.splitext(os.path.basename(self.path_text.get()))[0]+".mid")
                    self.open_files(output_file)    
        else:
            messagebox.showinfo("Error", "Please load a file before playing")

    def open_directory(self):
        """
        Open directory browse dialog to select a output directory.
        """
        # Show open directory dialog
        folder = filedialog.askdirectory(parent=self.master,title='Choose a folder')
        #self.output_path_text.set(folder.name)
        
        if folder != "":
            self.output_path_text.set(folder)
    
    def open_file(self):
        """
        Open file browse dialog to select a input file.
        """
        # Show open file dialog
        file = filedialog.askopenfile(parent=self.master,mode='rb',title='Choose a file')
        file_path = file.name
        self.path_text.set(os.path.normpath(file.name))

    def convert_file(self):
        """
        Start convertion of the selected input file. 
        """
        if self.path_text.get() != "":
            # Store input file from gui
            input_file = self.path_text.get()
            output_file = self.output_path_text.get()
            saveMidi = self.midi_checked.get()

            # Disable gui
            for ele in self.interact:
                ele['state'] = 'disabled'
            self.loading_msg['text'] = 'Converting...'
            
            # Create thread to convert format
            ThreadedConvert(self.queue, input_file, output_file, saveMidi).start()
            self.master.after(100, self.process_converted_queue)
        else:
            messagebox.showinfo("Error", "Please load a file before converting")

    def process_converted_queue(self): 
        """
        Function to wait for convertion results from Thread. The function istitle
        called every 100 ms.
        """
        # Check queue
        try:
            msg = self.queue.get(0)
            # Show result of the task if needed
            if(msg == "0"):
                messagebox.showinfo("Info", "File converted")
            elif(msg == "2"):
                messagebox.showinfo("Info", "Note out of range")
            else:
                messagebox.showinfo("Error", "File could not be converted")
            for ele in self.interact:
                ele['state'] = 'normal'
            self.loading_msg['text'] = ''
        except queue.Empty:
            self.master.after(100, self.process_converted_queue)

class ThreadedConvert(threading.Thread):
    def __init__(self, queue, input_file, output_file, saveMidi):
        """
        Initialize class to convert input file to machine format. The convertion
        is running in a Thread to unblock gui.
        
        Args:
            queue: message queue for communication
            input_file: input file to convert
            output_file: path for output file
            saveMidi: true if midi file should be generated
        """
        threading.Thread.__init__(self)
        self.queue = queue
        self.input_file = input_file
        self.output_file = output_file
        self.saveMidi = saveMidi
                    
    def convertDuration(self, duration, bpm):
        """
        Convert tiks duration of the note in seconds depending on bpm
        
        Args:
            duration: number of ticks 
            bpm: beats per minute
        
        Return duration of the note in seconds
        """
        return round(((60 / bpm) * duration), 2)  
        
    def run(self):
        """
        Main thrad for convertion
        """
        try:
            # Convert input to base format
            image_type, (bpm, format, in_range) = convertInputToBaseFormat(
                    self.input_file, 
                    self.output_file, 
                    saveMidi=self.saveMidi
                )

            if not in_range:
                self.queue.put("2")
                print("[SUCCESS] Notes out of range")
            else:
                print("[SUCCESS] all_good")
                self.queue.put("0")
        except Exception as ex:
            print("[ERROR] there was an error:", ex)
            self.queue.put("1")
 
# Create gui
root = Tk()
my_gui = XylonatorGUI(root, 400, 780)
root.mainloop()
