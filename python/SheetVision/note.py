from SheetVision.rectangle import Rectangle
import cv2
import math
import numpy as np

note_step = 0.0625 #0.0625

note_defs = {
     -4 : ("g5", 79),
     -3 : ("f5", 77),
     -2 : ("e5", 76),
     -1 : ("d5", 74),
      0 : ("c5", 72),
      1 : ("b4", 71),
      2 : ("a4", 69),
      3 : ("g4", 67),
      4 : ("f4", 65),
      5 : ("e4", 64),
      6 : ("d4", 62),
      7 : ("c4", 60),
      8 : ("b3", 59),
      9 : ("a3", 57),
     10 : ("g3", 55),
     11 : ("f3", 53),
     12 : ("e3", 52),
     13 : ("d3", 50),
     14 : ("c3", 48),
     15 : ("b2", 47),
     16 : ("a2", 45),
     17 : ("f2", 53),
     18 : ("f2", 53),
}

note_defs = {
     #-4 : ("g5", 79),
     #-3 : ("f5", 77),
     # 0 : ("e5", 76),
     # 1 : ("d5", 74),
      0 : ("c5", 72),
      1 : ("b4", 71),
      2 : ("a4", 69),
      3 : ("g4", 67),
      4 : ("f4", 65),
      5 : ("e4", 64),
      6 : ("d4", 62),
      7 : ("c4", 60),
      8 : ("b3", 59),
      9 : ("a3", 57),
     10 : ("g3", 55),
     11 : ("f3", 53),
     12 : ("e3", 52),
     13 : ("d3", 50),
     14 : ("c3", 48),
     15 : ("b2", 47),
     16 : ("a2", 45),
     17 : ("f2", 53)
}


class Note(object):
    def __init__(self, rec, sym, staff_rec, sharp_notes = [], flat_notes = [], debug_img=None):
        self.rec = rec
        self.sym = sym
 
        # Create list of virtual lines
        line_list = [] 
        step = staff_rec.h/16.0
        for i in range(18):
            line_list.append(staff_rec.y+(math.ceil(i*step)))
      
        # Compute distance between note middle and virtual line
        dist = []
        for l in line_list:
            dist.append(abs(rec.middle[1] - l))
        min = np.argmin(dist)
        
        # DEBUG
        """if debug_img is not None:
            if min % 2:
                cv2.line(debug_img, (0, line_list[min]), (staff_rec.w, line_list[min]), (0,0,255), 1)
            else:
                cv2.line(debug_img, (0, line_list[min]), (staff_rec.w, line_list[min]), (255,0,0), 1)
            rec.draw(debug_img, (255,0,0), 2)
            cv2.imshow("blubb", debug_img)
            cv2.waitKey(0)""" 

        # Get note definition
        note_def = note_defs[min]
        self.note = note_def[0]
        self.pitch = note_def[1]
        
        # Handel sharp and flat notes
        if any(n for n in sharp_notes if n.note[0] == self.note[0]):
            self.note += "#"
            self.pitch += 1
        if any(n for n in flat_notes if n.note[0] == self.note[0]):
            self.note += "b"
            self.pitch -= 1


