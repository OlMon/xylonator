import cv2
#import matplotlib.pyplot as plt
import numpy as np

'''
Find locations of templates in the musik sheet image.  Search for 
template with different sizes. 

@param img ist the musik sheet
@param templates is the templateimage
@param start is the min scale
@param stop is the max scale
@param threshold defines the deviation from the most found scale
@return best_locations is the list of all found lokations
@return scale of the template that was found in the music sheet image
'''
def fit(img, templates, start_percent, stop_percent, threshold, verbose=False):
    img_width, img_height = img.shape[::-1]
    best_location_count = -1
    best_locations = []
    best_scale = 1
    scale_factor = 2

    #plt.axis([0, 2, 0, 1])
    #plt.show(block=False)

    x = []
    y = []
    # search for template with different scales
    for scale in [i/100.0 for i in range(start_percent, stop_percent + 1, scale_factor)]:
        locations = []
        location_count = 0
        # search all templates in list for same music sign
        for template in templates:
            # resize template with actual scale
            template = cv2.resize(template, None,
                fx = scale, fy = scale, interpolation = cv2.INTER_CUBIC)
            # search for tamplate in actual scale in musik sheet image
            result = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)
            result = np.where(result >= threshold)
            # increment counter for founs results
            location_count += len(result[0])
            # add result to result list
            locations += [result]
        if verbose:
            print("scale: {0}, hits: {1}".format(scale, location_count))
        x.append(location_count)
        y.append(scale)
        #plt.plot(y, x)
        #plt.pause(0.00001)
        if (location_count > best_location_count):
            best_location_count = location_count
            best_locations = locations
            best_scale = scale
            #plt.axis([0, 2, 0, best_location_count])
        elif (location_count < best_location_count):
            pass
    #plt.close()

    return best_locations, best_scale