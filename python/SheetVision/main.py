import sys
import subprocess
import cv2
import time
import os
import numpy as np
from SheetVision.best_fit import fit
from SheetVision.rectangle import Rectangle
from SheetVision.note import Note
from random import randint
from midiutil.MidiFile3 import MIDIFile

# path to music character templates
quarter_files = [
    "SheetVision/resources/template/quarter.png", 
    "SheetVision/resources/template/solid-note.png"]
sharp_files = [
    "SheetVision/resources/template/sharp.png"]
flat_files = [
    "SheetVision/resources/template/flat-line.png", 
    "SheetVision/resources/template/flat-space.png" ]
half_files = [
    "SheetVision/resources/template/half-space.png", 
    "SheetVision/resources/template/half-note-line.png",
    "SheetVision/resources/template/half-line.png", 
    "SheetVision/resources/template/half-note-space.png"]
whole_files = [
    "SheetVision/resources/template/whole-space.png", 
    "SheetVision/resources/template/whole-note-line.png",
    "SheetVision/resources/template/whole-line.png", 
    "SheetVision/resources/template/whole-note-space.png"]

# load music character templates in separate lists
quarter_imgs = [cv2.imread(quarter_file, 0) for quarter_file in quarter_files]
sharp_imgs = [cv2.imread(sharp_files, 0) for sharp_files in sharp_files]
flat_imgs = [cv2.imread(flat_file, 0) for flat_file in flat_files]
half_imgs = [cv2.imread(half_file, 0) for half_file in half_files]
whole_imgs = [cv2.imread(whole_file, 0) for whole_file in whole_files]

# define min/max scale for character templates
sharp_lower, sharp_upper, sharp_thresh = 35, 150, 0.70
flat_lower, flat_upper, flat_thresh = 35, 150, 0.77
quarter_lower, quarter_upper, quarter_thresh = 35, 150, 0.70
half_lower, half_upper, half_thresh = 35, 150, 0.70
whole_lower, whole_upper, whole_thresh = 35, 150, 0.70

'''
Find locations of templates in the musik sheet image.  Search for 
template with different sizes. 

@param img ist the musik sheet
@param templates is the templateimage
@param start is the min scale
@param stop is the max scale
@param threshold defines the deviation from the most found scale
@return img_locations as a list of all found locations of the template
'''
def locate_images(img, templates, start, stop, threshold, verbose=True):
    locations, scale = fit(img, templates, start, stop, threshold, verbose=verbose)
    img_locations = []
    for i in range(len(templates)):
        w, h = templates[i].shape[::-1]
        w *= scale
        h *= scale
        img_locations.append([Rectangle(pt[0], pt[1], w, h) for pt in zip(*locations[i][::-1])])
    return img_locations

def merge_recs(recs, threshold):
    filtered_recs = []
    while len(recs) > 0:
        r = recs.pop(0)
        recs.sort(key=lambda rec: rec.distance(r))
        merged = True
        while(merged):
            merged = False
            i = 0
            for _ in range(len(recs)):
                if r.overlap(recs[i]) > threshold or recs[i].overlap(r) > threshold:
                    r = r.merge(recs.pop(i))
                    merged = True
                elif recs[i].distance(r) > r.w/2 + recs[i].w/2:
                    break
                else:
                    i += 1
        filtered_recs.append(r)
    return filtered_recs

def open_file(path):
    os.startfile(path)
    
def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]
        
#if __name__ == "__main__":
def sheet_vision(img_file, output_path="", verbose=False):
    # Load file from disc
    img = cv2.imread(img_file, 0)
    img_gray = img#cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.cvtColor(img_gray, cv2.COLOR_GRAY2RGB)
    ret,img_gray = cv2.threshold(img_gray, 127, 255, cv2.THRESH_BINARY)
    
    # get size of music sheet image
    img_width, img_height = img_gray.shape[::-1]

    ''' ---------------------------------------------------------------------------------------'''
    ''' Find staff locations in image                                                          '''
    ''' ---------------------------------------------------------------------------------------'''
    
    # Convert image to binary
    gray = img_gray
    ret, binary_img = cv2.threshold(gray,128,255,cv2.THRESH_BINARY)

    # Edge detection 
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    
    lines = cv2.HoughLinesP(
            edges,
            1, np.pi / 180,100,
            minLineLength = img.shape[0]*0.55,
            maxLineGap = img.shape[0]*0.025
        )

    # Remove outer list
    lines = np.squeeze(lines)
    
    # Sort lines in y direction
    sorted_lines = sorted(lines, key=lambda tup: tup[1])    
 
    detected_lines = [sorted_lines[0]]   
    for i in range(1, len(sorted_lines)):
        if ((sorted_lines[i][1] - sorted_lines[i-1][1]) > 5):
            detected_lines.append(sorted_lines[i])
  
    # Print all detected lines
    if verbose:
        print("detected_lines...\n")
        for (i, line) in enumerate(detected_lines):
            x1,y1,x2,y2 = line
            cv2.line(img,(x1,y1),(x2,y2),(0,255,0),1)
 
    # Split detected lines into chunks
    boxed_lines = chunks(detected_lines, 5)
    staff_boxes = []
    for b in boxed_lines:
        # Check if chuck split was successul
        if len(b) == 5:
            # Extract outer points
            left_top_point = (b[0][0], b[0][1] - int((b[4][3] - b[0][1])*0.5))
            bottom_right_point = (b[4][2], b[4][3] + int((b[4][3] - b[0][1])*0.5))
            
            # Create rectangle class
            rect = Rectangle(
                    left_top_point[0], 
                    left_top_point[1], 
                    bottom_right_point[0]-left_top_point[0],
                    bottom_right_point[1]-left_top_point[1],
                )
                
            # Draw rectangle on image
            cv2.rectangle(img, (left_top_point[0], left_top_point[1]), 
                (bottom_right_point[0], bottom_right_point[1]), (255,0,0), 2)
            staff_boxes.append(rect)

    if verbose:
        print("Matching staff image...")
    
    staff_recs = []
    for box in staff_boxes:
        # Extract roi from base image
        roi = img_gray[box.y:box.y+box.h , 0:img_gray.shape[1]]
        ret, roi = cv2.threshold(roi, 127, 255, cv2.THRESH_BINARY)
        #roi = cv2.morphologyEx(roi, cv2.MORPH_CLOSE, np.ones((5,1),np.uint8))
        roi = cv2.morphologyEx(roi, cv2.MORPH_OPEN, np.ones((5,5),np.uint8))
        roi = cv2.morphologyEx(roi, cv2.MORPH_CLOSE, np.ones((5,1),np.uint8))
        roi = cv2.morphologyEx(roi, cv2.MORPH_OPEN, np.ones((5,5),np.uint8))
        roi = cv2.morphologyEx(roi, cv2.MORPH_CLOSE, np.ones((5,1),np.uint8))
        
        border = 2
        
        # Add border to image
        roi[0:roi.shape[1], 0:border] = 255
        roi[0:roi.shape[1], roi.shape[1]-border:roi.shape[1]] = 255
        roi[0:border, 0:roi.shape[1]] = 255
        roi[roi.shape[0]-border:roi.shape[0], 0:roi.shape[1]] = 255
       
        # Find contours, part 1
        im2, contours, hierarchy = cv2.findContours(roi, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for i in contours:
            x,y,w,h = cv2.boundingRect(i)
            if w == roi.shape[1]:
                continue
            cv2.line(roi, (x,0), (x,roi.shape[0]), (0,255,0), 20)
            cv2.rectangle(roi,(x,y),(x+w,y+h),(0,255,0),-1)
       
        # Add border to image
        roi[0:roi.shape[1], 0:border] = 255
        roi[0:roi.shape[1], roi.shape[1]-border:roi.shape[1]] = 255
        roi[0:border, 0:roi.shape[1]] = 255
        roi[roi.shape[0]-border:roi.shape[0], 0:roi.shape[1]] = 255
       
        # Find contours, part 2
        boxes = []
        im2, contours, hierarchy = cv2.findContours(roi, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for i in contours:
            # Get bounding rect from note
            x,y,w,h = cv2.boundingRect(i)
            if w == roi.shape[1]:
                continue
            cv2.rectangle(roi,(x,y),(x+w,y+h),(0,255,0),-1)
            boxes.append((x, y, w, h))
            
        # Sort boxes in x direction
        sorted_boxes = sorted(boxes, key=lambda tup: tup[0])    
     
        # Create staff rects
        staff_recs_boxes = []
        for (x, y, w, h) in sorted_boxes:
            rect = Rectangle(x, box.y, w, box.h)
            staff_recs_boxes.append(rect)
        staff_recs.append(staff_recs_boxes)

    if verbose:
        for r in staff_recs:
            for b in r:
                # Draw staff rect in image
                b.draw(img, (0, 0, 255), 2)

    ''' ---------------------------------------------------------------------------------------'''
    ''' Find whole note, half note, quarter note, sharp, flat and bar rest locations in image  '''
    ''' ---------------------------------------------------------------------------------------'''

    scale = int((h/150)*100)
    scale_min = int(scale - scale * 0.15)
    scale_max = int(scale + scale * 0.20)
    
    # define min/max scale for character templates
    sharp_lower, sharp_upper, sharp_thresh = scale_min, scale_max, 0.77
    flat_lower, flat_upper, flat_thresh = scale_min, scale_max, 0.73
    quarter_lower, quarter_upper, quarter_thresh = scale_min, scale_max, 0.7
    half_lower, half_upper, half_thresh = scale_min, scale_max, 0.60
    whole_lower, whole_upper, whole_thresh = scale_min, scale_max, 0.70
    
    
    if verbose:
        print("Matching sharp image...")
    sharp_recs = locate_images(img_gray, sharp_imgs, sharp_lower, sharp_upper, sharp_thresh, verbose=verbose)
    sharp_recs = merge_recs([j for i in sharp_recs for j in i], 0.5)
    if verbose:
        print("Merging sharp image results...")
        for r in sharp_recs:
            r.draw(img, (255, 0, 255), 2)

    if verbose:
        print("Matching flat image...")
    flat_recs = locate_images(img_gray, flat_imgs, flat_lower, flat_upper, flat_thresh, verbose=verbose)
    flat_recs = merge_recs([j for i in flat_recs for j in i], 0.5)
    if verbose:
        print("Merging flat image results...")
        for r in flat_recs:
            r.draw(img, (0, 255, 255), 2)

    if verbose:
        print("Matching quarter image...")
    quarter_recs = locate_images(img_gray, quarter_imgs, quarter_lower, quarter_upper, quarter_thresh, verbose=verbose)
    quarter_recs = merge_recs([j for i in quarter_recs for j in i], 0.5)
    if verbose:
        print("Merging quarter image results...")
        for r in quarter_recs:
            r.draw(img, (255, 255, 0), 2)

    if verbose:
        print("Matching half image...")
    half_recs = locate_images(img_gray, half_imgs, half_lower, half_upper, half_thresh, verbose=verbose)
    half_recs = merge_recs([j for i in half_recs for j in i], 0.5)
    if verbose:
        print("Merging half image results...")
        for r in half_recs:
            r.draw(img, (0, 0, 255), 2)

    if verbose:
        print("Matching whole image...")
    whole_recs = locate_images(img_gray, whole_imgs, whole_lower, whole_upper, whole_thresh, verbose=verbose)
    whole_recs = merge_recs([j for i in whole_recs for j in i], 0.5)
    if verbose:
        print("Merging whole image results...")
        for r in whole_recs:
            r.draw(img, (0, 255, 0), 2)

    ''' ---------------------------------------------------------------------------------------'''
    ''' Check position of note groups in staff boxes with sharp  and flat                      '''
    ''' ---------------------------------------------------------------------------------------'''

    note_groups = []
    if verbose:
        print("box in staff_boxes...")
    for (i, box) in enumerate(staff_boxes):
        staff_sharps = [Note(r, "sharp", box) 
            for r in sharp_recs if abs(r.middle[1] - box.middle[1]) < box.h*5.0/8.0]
            
        staff_flats = [Note(r, "flat", box) 
            for r in flat_recs if abs(r.middle[1] - box.middle[1]) < box.h*5.0/8.0]

        quarter_notes = [Note(r, "4,8", box, staff_sharps, staff_flats) 
            for r in quarter_recs if abs(r.middle[1] - box.middle[1]) < box.h*5.0/8.0]
        
        half_notes = [Note(r, "2", box, staff_sharps, staff_flats) 
            for r in half_recs if abs(r.middle[1] - box.middle[1]) < box.h*5.0/8.0]
        
        whole_notes = [Note(r, "1", box, staff_sharps, staff_flats) 
            for r in whole_recs if abs(r.middle[1] - box.middle[1]) < box.h*5.0/8.0]
        
        staff_notes = quarter_notes + half_notes + whole_notes
        staff_notes.sort(key=lambda n: n.rec.x)

        # Get staff rects from current box
        staffs = staff_recs[i]
        
        for s in staffs:
            note_color = (randint(0, 255), randint(0, 255), randint(0, 255))
            note_group = []
            for n in staff_notes:
                if s.overlap(n.rec):
                    note_group.append(n)
                    n.rec.draw(img, note_color, 2)
            if note_group != []:
                note_groups.append(note_group)
        
    ''' ---------------------------------------------------------------------------------------'''
    ''' Show debug                                                                             '''
    ''' ---------------------------------------------------------------------------------------'''

    if verbose:
        # Print detected notes and note groups
        print("-------------------------")
        print("notes in note groups:")
        for note_group in note_groups:
            print("-------------------------")
            print([ note.note + " " + note.sym for note in note_group])
            
        # Show debug file
        output_path_ext = ""
        if output_path != "":
            output_path_ext = output_path + "/"
        cv2.imwrite(output_path_ext + 'res.png', img)
        open_file(output_path_ext + 'res.png')
      
    return note_groups