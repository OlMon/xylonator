# Import the necessary packages
from os import path

# Define the path to the output directory used for storing plots, classification reports, ...
OUTPUT_PATH = "../output/"
OUTPUT_FORMAT_1 = path.sep.join([OUTPUT_PATH, "FORMAT1"])

# Default value for beats per minute
BPM = 140

# Define available notes and their coordinates
NOTE_COORDINATES = [
    {'note': 0, 'x': 0, 'y': 0, 'z': 0},
    {'note': 1, 'x': 15, 'y': 15, 'z': 15},
    {'note': 2, 'x': 15, 'y': 15, 'z': 15},
    {'note': 3, 'x': 15, 'y': 15, 'z': 15},
    {'note': 4, 'x': 15, 'y': 15, 'z': 15},
    {'note': 5, 'x': 15, 'y': 15, 'z': 15},
    {'note': 6, 'x': 15, 'y': 15, 'z': 15},
    {'note': 7, 'x': 15, 'y': 15, 'z': 15},
    {'note': 8, 'x': 15, 'y': 15, 'z': 15},
    {'note': 9, 'x': 15, 'y': 15, 'z': 15},
    {'note': 10, 'x': 15, 'y': 15, 'z': 15},
    {'note': 11, 'x': 15, 'y': 15, 'z': 15},
    {'note': 12, 'x': 15, 'y': 15, 'z': 15},
    {'note': 13, 'x': 15, 'y': 15, 'z': 15},
    {'note': 14, 'x': 15, 'y': 15, 'z': 15},
    {'note': 15, 'x': 15, 'y': 15, 'z': 15},
    {'note': 16, 'x': 15, 'y': 15, 'z': 15},
    {'note': 17, 'x': 15, 'y': 15, 'z': 15},
    {'note': 18, 'x': 15, 'y': 15, 'z': 15},
    {'note': 19, 'x': 15, 'y': 15, 'z': 15},
    {'note': 20, 'x': 15, 'y': 15, 'z': 15},
    {'note': 21, 'x': 15, 'y': 15, 'z': 15},
    {'note': 22, 'x': 15, 'y': 15, 'z': 15},
    {'note': 23, 'x': 15, 'y': 15, 'z': 15},
    {'note': 24, 'x': 15, 'y': 15, 'z': 15},
    {'note': 25, 'x': 15, 'y': 15, 'z': 15},
    {'note': 26, 'x': 15, 'y': 15, 'z': 15},
    {'note': 27, 'x': 15, 'y': 15, 'z': 15}
]
NOTE_COUNT = len(NOTE_COORDINATES)

def getNoteConfiguration(note):
    """
    Get configuration (e.g. coordinates) of a given note.
    
    Args:
        note: Name of note to search for
    
    Return dictionary of note configuration
    """
    #return (item for item in NOTE_COORDINATES if item["name"] == note).next()
    return [element for element in NOTE_COORDINATES if element['note'] == note][0]