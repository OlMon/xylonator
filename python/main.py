# Import the necessary packages
from config import xylonator as config
from converter import *

import SheetVision.main as sv
import sys
import os
import argparse
    
def convertDuration(duration, bpm):
    """
    Convert tiks duration of the note in seconds depending on bpm
    
    Args:
        duration: number of ticks 
        bpm: beats per minute
    
    Return duration of the note in seconds
    """
    return round(((60 / bpm) * duration), 2)  
 
if __name__ == "__main__":
    # Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--input", required=True,
        help="path to input file")
    args = vars(ap.parse_args())

    # Convert input to base format
    image_type, (bpm, base_format) = convertInputToBaseFormat(args["input"], config.OUTPUT_PATH)
    print("[INFO] reading input image... type={}".format(image_type[1:]))
    print("[INFO] convert input to base format... bpm={}".format(bpm))
    
    # Convert base format to machine format
    machine_format =  []
    for f in base_format:
        # Get note and duration from converted base format
        bpm = 160
        note, duration = f['note'], convertDuration(f['duration'], bpm)

        # Validate note range
        if not isinstance(note, int) or note not in range(0, config.NOTE_COUNT):
            print("[ERROR] note {} unknown... ".format(note))
            break
        
        # Validate duration
        if not isinstance(duration, int) and not isinstance(duration, float):
            print("[ERROR] duration is has no valid value... ".format(duration))
        
        # Get note position from global configuration and build machine format
        note_position = config.getNoteConfiguration(int(f['note']))
        
        f['duration'] = duration
        
        print(f)
        
        f.update(note_position)
        machine_format.append(f)
    print("[INFO] convert base format to machine format... ")
             
    # DUMP MACHINE FORMAT
    #time_song = 0
    #for m in machine_format:
    #    time_song += m['duration']
    #    print(m)
    #
    #print(time_song)
