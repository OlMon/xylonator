# Import the necessary packages
import SheetVision.main as sv
import os
import midi
import kukaheader
        
from config import xylonator as config
from midiutil.MidiFile3 import MIDIFile

# -- XYLONATOR -----------------------------------------------------------------------

def dumpXylonatorFormatToFile(data, bpm, output_path, file_name):
    """
    Dump xylonator format to file.
    
    Args:
        data: List of xylonator notes
        output_path: Path to output directory
    """
    # Remove file if it's already exists
    if os.path.exists(output_path):
        os.remove(output_path)

    in_range = True
        
    print("[INFO] convert base format to machine format... ")            
    with open(output_path, 'w') as f:
        f.write(kukaheader.HEADER1)
        f.write(file_name)
        f.write(kukaheader.HEADER2)
    
        time_file = 0;
        for idx, d in enumerate(data):            
            if d['note'] != 0:
                f.write("PTP Xt{}\n".format(d['note']))
            
            if (idx > 0):
                f.write("wait for $TIMER[1]>0\n")

            if d['note'] != 0:
                f.write("PULSE($OUT[1], TRUE, 0.1)\n")
                
            f.write("$TIMER[1] = {}\n\n".format(round((60 / bpm) * d['duration'] * -1000)))
            time_file += (60 / bpm) * d['duration']
            
            if d['note'] == 0:
                in_range = False
                        
        print("[INFO] time of song in seconds... time =", time_file)
            
        f.write(kukaheader.HEADER3)
        
    return in_range


def loadXylonatorFile(input_file, output_path="", saveMidi=False):
    """
    Load xylonator format.
    
    Args:
        input_file: Path to input file
        output_path: Path to output directory
    
    Return xylonator format
    """
    # Get type of the music sheet file
    base_file, image_type = os.path.splitext(input_file)

    # Set default bpm
    bpm = config.BPM
    format = []
    
    # Open ultrastar file
    with open(input_file) as f:
        # Read all lines
        content = f.readlines()

        # Parse line by line and ignore first one because it's a header
        for c in content[1:]:
            # Remote trailing newline
            line = c.rstrip()

            # If line starts with '#' we can ignore it
            if line[0] == '#':
                if line.startswith("#BPM"):
                    bpm = line.split(" ")[1]
                continue

            note, duration = line.split(" ")
            format.append({'note': int(note), 'duration': float(duration)})
            
        # Dump format to file
        output_file = os.path.join(output_path, os.path.basename(base_file) + ".src")
        in_range = dumpXylonatorFormatToFile(format, bpm, output_file, os.path.basename(base_file))
        
    # Return format
    return (bpm, format, in_range)

# -- SHEETVISION -----------------------------------------------------------------------
            
def convertSheetvisionNoteToXylonatorNote(note):
    """
    Convert sheet vision note to xylonator note.
    
    Args: 
        note: Input note from sheet vision
        
    Return converted xylonator note
    """
    
    try :
        LOOKUP_TABLE = {
            'g3'  : 1,  'g3#' : 2,
            'a3b' : 2,  'a3'  : 3, 'a3#'  : 4,
            'b3b' : 4,  'b3'  : 5,
            'c3'  : 6,  'c3#' : 7,
            'd3b' : 7,  'd3'  : 8, 'd3#'  : 9,
            'e3b' : 9,  'e3'  : 10, 
            'f3'  : 11, 'f3#' : 12,
            'g3b' : 12, 'g4'  : 13, 'g4#' : 14,
            'a4b' : 14, 'a4'  : 15, 'a4#' : 16,
            'b4b' : 16, 'b4'  : 17,
            'c4'  : 18, 'c4#' : 19,
            'd4b' : 19, 'd4'  : 20, 'd4#' : 21,
            'e4b' : 21, 'e4'  : 22, 
            'f4'  : 23, 'f4#' : 24,
            'g4b' : 24, 'g5'  : 25, 'g5#' : 26,
            'a5b' : 26, 'a5'  : 27,
        }
        return LOOKUP_TABLE[str(note)]
    except:
        return 0
        
# Define the function blocks
def convertSheetvisionToXylonatorFormat(input_file, output_path="", saveMidi=False):
    """
    Convert sheet vision to xylonator format.
    
    Args:
        input_file: Path to input file
        output_path: Path to output directory
    
    Return xylonator format
    """
    # Get type of the music sheet file
    base_file, image_type = os.path.splitext(input_file)

    # Get note groups from sheet vision module
    note_groups = sv.sheet_vision(input_file, output_path, verbose=True)

    # Set default bpm
    bpm = config.BPM

    # Create midi file 
    if saveMidi:
        track = 0; time = 0; channel = 0; volume = 100
        midi = MIDIFile(1)
        midi.addTrackName(track, time, "Track")
        midi.addTempo(track, time, bpm)
    
    # Parse all found note groups
    format = []
    for note_group in note_groups:
        for note_info in note_group:
            # Convert sheet vision note to xylonator note
            note = convertSheetvisionNoteToXylonatorNote(note_info.note)

            # Get duration from shee vision note
            if note_info.sym == "1":
                duration = 4
            elif note_info.sym == "2":
                duration = 2
            elif note_info.sym == "4,8":
                duration = 1 if len(note_group) == 1 else 0.5
                
            format.append({'note': note, 'duration': duration})

            # Add note to midi file
            if saveMidi:
                pitch = note_info.pitch
                midi.addNote(track, channel, pitch, time, duration, volume)
                time += duration
            
        # DUMP
        # print([ note.note + " " + note.sym for note in note_group])
    # Save midi file
    if saveMidi:
        output_file = os.path.join(output_path, os.path.basename(base_file) + ".mid")
        if os.path.exists(output_file):
            os.remove(output_file)
        binfile = open(output_file, 'wb')
        midi.writeFile(binfile)
        binfile.close()
        
    # Dump format to file
    output_file = os.path.join(output_path, os.path.basename(base_file) + ".src")
    in_range = dumpXylonatorFormatToFile(format, bpm, output_file, os.path.basename(base_file))
    
    # Return format
    return (bpm, format, in_range)
    
# -- MIDI -----------------------------------------------------------------------

def convertMidiNoteToXylonatorNote(pitch, velocity):
    """
    Convert midi pitch/velocity to xylonator note.
    
    Args: 
        pitch: Input pitch from midi file
        velocity: Input velocity from midi file
        
    Return converted xylonator note
    """
    return (pitch-42) % config.NOTE_COUNT

def convertMidiToXylonatorFormat(input_file, output_path="", saveMidi=False):
    """
    Convert midi to xylonator format.
    
    Args:
        input_file: Path to input file
        output_path: Path to output directory
    
    Return midi format
    """
    # Get type of the music sheet file
    base_file, image_type = os.path.splitext(input_file)

    # Set default bpm
    bpm = config.BPM
    mpqn = 60*1000000/config.BPM
    
    # Read midi file
    pattern = midi.read_midifile(input_file)

    # Dump converted midi to file
    if saveMidi:
        track_o=0; time_o=0; channel_o=0; volume_o=100    
        midi_o = MIDIFile(1)
        midi_o.addTrackName(track_o, time_o, "Track")
        midi_o.addTempo(track_o, time_o, bpm)
    
    #print("[PATTERN] convertMidiToXylonatorFormat... bpm={}".format(pattern))
        
    format = []
    for tracks in pattern:
        start = 0
        pitch = 0
        velocity = 0
        
        #print("[TRACKS] convertMidiToXylonatorFormat... bpm={}".format(tracks))        
        #print(tracks)
        
        # Loop over each track
        for t in tracks:
            if isinstance(t,midi.SetTempoEvent):
                bpm = t.get_bpm()
                mpqn = t.get_mpqn()
                
                # Dump converted midi to file
                if saveMidi:
                    midi_o.addTempo(track_o, time_o, bpm)

            #elif isinstance(t,midi.NoteOnEvent) and (t.channel == 1):
            #   current = t.channel
            #   print(current)

            elif isinstance(t,midi.NoteOffEvent) and (t.channel == 0):
                current = t.channel
                # Get tempo in microseconds per beat
                tempo = mpqn
                # Convert into milliseconds per beat
                tempo = tempo / 1000.0
                # Generate ms per tick
                mpt = tempo / pattern.resolution
                # Generate s per tick
                spt = mpt * 0.001
                # Compute duration of note and convert it to length of note
                duration = spt * t.tick / 60 * bpm
                
                if (duration > 0):
                    # Compute note          
                    pitch = t.get_pitch()
                    velocity = t.get_velocity()
                    note = convertMidiNoteToXylonatorNote(pitch, velocity)
                    
                    format.append({'note': note, 'duration': duration})
                    
                    # Dump converted midi to file
                    if saveMidi:
                        midi_o.addNote(track_o,channel_o,pitch,time_o,duration,volume_o)
                        time_o += duration
                
    # Dump converted midi to file
    if saveMidi:
        output_file = os.path.join(output_path, os.path.basename(base_file) + ".mid")
        if os.path.exists(output_file):
            os.remove(output_file)
        binfile = open(output_file, 'wb')
        midi_o.writeFile(binfile)
        binfile.close()

    # Dump format to file
    output_file = os.path.join(output_path, os.path.basename(base_file) + ".src")
    in_range = dumpXylonatorFormatToFile(format, bpm, output_file, os.path.basename(base_file))

    # print("BPM: ", bpm)
    
    return (bpm, format, in_range)
    
# -- ULTRASTAR -----------------------------------------------------------------------

def convertUltrastarNoteToXylonatorNote(note):
    """
    Convert ultrastar note to xylonator note.
    
    Args: 
        note: Input note from ultrastar
        
    Return converted xylonator note
    """
    # TODO
    return (int(note)+6+11) % config.NOTE_COUNT

def convertUltrastarToXylonatorFormat(input_file, output_path="", saveMidi=False):
    """
    Convert ultrastar to xylonator format.
    
    Args:
        input_file: Path to input file
        output_path: Path to output directory
    
    Return xylonator format
    """
       
    # Get type of the music sheet file
    base_file, image_type = os.path.splitext(input_file)

    # Open ultrastar file
    with open(input_file) as f:
        # Read all lines
        content = f.readlines()
        bpm = config.BPM

        # Parse line by line
        notes = []
        for c in content:
            # Remove trailing newline
            line = c.rstrip()
            
            # If line starts with '#' we can ignore it
            if line[0] == '#':
                # If line starts with #BMP: then read bmp
                if line.startswith("#BPM"):
                    bpm = int(float(line.split(":")[1].replace(",", ".")))
                continue
              
            split = line.split(" ")
            if len(split) != 5:
                continue
            
            # Create templist: timestamp, note, length of the note
            notes.append(split[1:4])

        # Parse notes to compute base format
        format = []
        for i in range(0, len(notes), 1):
            # Convert ulrastar note to xylonator note
            note = convertUltrastarNoteToXylonatorNote(notes[i][2])
            
            # We've to calculate the duration with the current and the next note 
            if i > len(notes)-2:
                format.append({'note': note, 'duration': float(notes[i][1])})
                break
            format.append({'note': note, 'duration': (float(notes[i+1][0]) - float(notes[i][0])) / 4})

        # Dump format to file
        output_file = os.path.join(output_path, os.path.basename(base_file) + ".src")
        in_range = dumpXylonatorFormatToFile(format, bpm, output_file, os.path.basename(base_file))
        
        # Return format
        return (bpm, format, in_range)

# -- BASE CONVERTER ----------------------------------------------------------------------

def convertInputToBaseFormat(input, output_path="", saveMidi=False):
    """
    Convert input to base format (e.g. ultrastar -> xylonator). The file type
    will be detected automatically. Supported formats are JPG, PNG, MIDI, TXT and SRC.
    
    Args:
        input: Path to input file
        output_path: Path to output directory (default="")
    
    Return detected image type and converted format
    """
    
    # Get type of the music sheet file
    image_type = os.path.splitext(input)[1]
    print("[INFO] reading input image... type={}".format(image_type[1:]))
    print("[INFO] convert input to base format...")

    # map the inputs to the function blocks
    options = {'.jpg' : convertSheetvisionToXylonatorFormat,
               '.png' : convertSheetvisionToXylonatorFormat,
               '.mid' : convertMidiToXylonatorFormat,
               '.txt' : convertUltrastarToXylonatorFormat,
               '.src' : loadXylonatorFile,
    }

    # Create output directory
    if not os.path.exists(output_path):
        os.makedirs(output_path)
        
    print(options)
    return image_type, options[image_type](input, output_path, saveMidi=saveMidi)